﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AntiVmDetection.Interface
{
    interface IDetection
    {
        /// <summary>
        /// 检测方法
        /// </summary>
        /// <returns>检测成功与否</returns>
        bool Detection();
    }
}
