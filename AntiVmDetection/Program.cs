﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using AntiVmDetection.Impl;
using AntiVmDetection.Utils;

namespace AntiVmDetection
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                VmwareDetection vmwareDetection = new VmwareDetection();
                if (vmwareDetection.Detection())
                    Console.WriteLine("Run in virtual machine!");
                vmwareDetection.PrintDetection();

            }catch(SecurityException)
            {
                Console.WriteLine("权限不足");
            }
            Console.ReadLine();
            //VmwareCheat vmwareDetection = new VmwareCheat();
            //vmwareDetection.Recover();
            //vmwareDetection.CheatDetection();
        }
    }
}
