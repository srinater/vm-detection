﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace AntiVmDetection.Utils
{
    class Winapi
    {
        public static readonly UInt32 HKEY_CLASSES_ROOT = 0x80000000;
        public static readonly UInt32 HKEY_CURRENT_USER = 0x80000001;
        public static readonly UInt32 HKEY_LOCAL_MACHINE = 0x80000002;
        public static readonly UInt32 HKEY_USERS = 0x80000003;
        public static readonly UInt32 HKEY_PERFORMANCE_DATA = 0x80000004;
        public static readonly UInt32 HKEY_PERFORMANCE_TEXT = 0x80000050;
        public static readonly UInt32 HKEY_PERFORMANCE_NLSTEXT = 0x80000060;
        public static readonly UInt32 HKEY_CURRENT_CONFIG = 0x80000005;
        public static readonly UInt32 HKEY_DYN_DATA = 0x80000006;
        public static readonly UInt32 HKEY_CURRENT_USER_LOCAL_SETTINGS = 0x80000007;

        [DllImport("Advapi32.dll", CharSet = CharSet.Unicode)]
        public static extern int RegOpenKey(UInt32 hKey, string lpSubKeyName, out UInt32 phKey);
        [DllImport("Advapi32.dll", CharSet = CharSet.Unicode)]
        public static extern int RegCloseKey(UInt32 hKey);
        [DllImport("Advapi32.dll", CharSet = CharSet.Unicode)]
        public static extern int RegRenameKey(UInt32 hKey, string lpSubKeyName, string lpNewKeyName);

        [DllImport("KernelInterface.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern byte InByte(ushort addr);
        [DllImport("KernelInterface.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void InByteString(ushort addr, byte[] @string, uint length);
        [DllImport("KernelInterface.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern ushort InWord(ushort addr);
        [DllImport("KernelInterface.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern uint InDword(ushort addr);
        [DllImport("KernelInterface.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void OutByte(ushort addr, byte data);
        [DllImport("KernelInterface.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void OutByteString(ushort addr, byte[] @string, ulong length);
        [DllImport("KernelInterface.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void OutWord(ushort addr, ushort data);
        [DllImport("KernelInterface.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void OutDword(ushort addr, uint data);
        [DllImport("KernelInterface.dll", CallingConvention=CallingConvention.Cdecl)]
        public static extern void Cpuid(int[] outInfo, int eax, int ecx);
        [DllImport("KernelInterface.dll", CallingConvention=CallingConvention.Cdecl)]
        public static extern bool VmwareCall(uint[] @out, ushort cmd, ushort param1, uint param2);
    }
}
